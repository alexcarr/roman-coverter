package test.roman;

import org.junit.*;
import static org.junit.Assert.*;
import roman.RomanConverter;

public class RomanTests {

	@Test
	public void romanConvert(){
	
		RomanConverter tester = new RomanConverter();
		
		//toRoman assert statements
		assertEquals("I", tester.toRoman(1));
		assertEquals("MDI", tester.toRoman(1501));
		
		//fromRoman assert statements
		assertEquals(1, tester.fromRoman("I"));
		assertEquals(1432,tester.fromRoman("MCDXXXII"));

	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidNumeralToRomanTest1(){
		RomanConverter tester = new RomanConverter();
		int romanToInt = tester.fromRoman("ABCSS");
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidNumeralToRomanTest2(){
		RomanConverter tester = new RomanConverter();
		int romanToInt = tester.fromRoman("IVVVMMI");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void invalidNumeralTestToRoman(){
		RomanConverter tester = new RomanConverter();
		String roman = tester.toRoman(4001);
	}

	@Test(expected = IllegalArgumentException.class)
	public void negativeNumeralToRomanTest(){
		RomanConverter tester = new RomanConverter();
		String roman = tester.toRoman(-40);
	}
}
